#include <iostream>
#include <string>
#include <string.h>
#include <errno.h>
#include <stdio.h>
#include <vector>

#include "event2/bufferevent.h"
#include "event2/buffer.h"
#include "event2/listener.h"
#include "event2/util.h"
#include "event2/event.h"
#include <event2/event-config.h>
#include <WinSock2.h>

using namespace std;

#define IP_ADDRESS ("127.0.0.1")
#define PORT (7788)

string mybuff;

//发送数据格式
struct Header
{
	int				m_intPackSize;	//传送包大小
	int				m_intPackNo;	//包序号
};

struct Body
{
	Body()
	{
		memset(this, 0, sizeof(Body));
	}
	int DataSize;		//数据包大小
	UINT32 mDate;
	UINT32 mTime;
	char Name[16];
	char Code[32];
};


int m_isrun = 0;
int tcp_connect_server(const char* server_ip, int port);
void cmd_msg_cb(int fd, char* msg);
void socket_read_cb(int fd, short events, void *arg);

DWORD WINAPI Fun1Proc(LPVOID lpParameter)
{
	char t_cin[1024];
	int sockfd = (int)lpParameter;
	while (1) {
		memset(t_cin, 0, 1024);

		std::cin >> t_cin;

		if (strcmp(t_cin, "exit") == 0) {
			break;
		}
		cmd_msg_cb(sockfd, t_cin);
	}
	exit(1);
	return 0;
}
int main(int argc, char** argv)
{
	//加载套接字库  
	WSADATA wsaData;
	int iRet = 0;
	iRet = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iRet != 0)
	{
		return -1;
	}

	if (2 != LOBYTE(wsaData.wVersion) || 2 != HIBYTE(wsaData.wVersion))
	{
		WSACleanup();
		return -1;
	}
	
	int sockfd = tcp_connect_server(IP_ADDRESS, PORT);
	if (sockfd == -1)
	{
		perror("tcp_connect error ");
		return -1;
	}
	printf("connect to server successful\n");
	struct event_base* base = event_base_new();
	struct event *ev_sockfd = event_new(base, sockfd, EV_READ | EV_PERSIST, socket_read_cb, NULL);
	event_add(ev_sockfd, NULL);
	//创建线程发送数据
	HANDLE hThread1 = CreateThread(NULL, 0, Fun1Proc, (void*)sockfd, 0, NULL);
	CloseHandle(hThread1);
	event_base_dispatch(base);
	WSACleanup();
	printf("finished \n");
	return 0;
}
void cmd_msg_cb(int fd, char* msg)
{
	//把终端的消息发送给服务器端  
	int ret = send(fd, (const char*)msg, strlen((char*)msg), 0);
	if (ret <= 0)
	{
		perror("read fail ");
		return;
	}
	printf("send:%s\n", (char*)msg);
}

void PreProcessData(string strMsg, unsigned short nLen)
{
	if (nLen <= sizeof(Header))
	{
		return;
	}
	Header* pHead = (Header*)(strMsg.data());
	Body * pBody = (Body*)((char *)pHead + sizeof(Header));
	cout << "PacketSize:" <<pHead->m_intPackSize<<",PacketNo:"<<pHead->m_intPackNo<< endl;
	cout << "DataSize:"<<pBody->DataSize<<",Code:"<<pBody->Code << ",Name:" <<pBody->Name<<",Date:"<<pBody->mDate<<",Time:"<<pBody->mTime<<endl;
}

void socket_read_cb(int fd, short events, void *arg)
{
	char msg[1024];
	int len = recv(fd, msg, sizeof(msg) - 1, 0);
	if (len <= 0)
	{
		perror("read fail ");
		exit(1);
	}
	
	mybuff.append(msg, len);
	PreProcessData(mybuff, len);
	mybuff.clear();
}
int tcp_connect_server(const char* server_ip, int port)
{
	int sockfd, status, save_errno;
	SOCKADDR_IN  server_addr;

	memset(&server_addr, 0, sizeof(server_addr));

	server_addr.sin_family = AF_INET;
	server_addr.sin_addr.s_addr = inet_addr(server_ip);
	server_addr.sin_port = htons(port);

	sockfd = ::socket(PF_INET, SOCK_STREAM, 0);
	if (sockfd == -1)
		return sockfd;


	status = ::connect(sockfd, (struct sockaddr*)&server_addr, sizeof(server_addr));

	if (status == -1)
	{
		save_errno = errno;   //清理  
		closesocket(sockfd);
		errno = save_errno; //the close may be error  
		return -1;
	}

	evutil_make_socket_nonblocking(sockfd);

	return sockfd;
}
