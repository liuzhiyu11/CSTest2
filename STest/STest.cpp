#include <stdio.h>  
#include <iostream>
#include <string.h>  
#include <errno.h>
#include <signal.h>
#include <vector>
#include <ctime>

#include "event2/bufferevent.h"
#include "event2/buffer.h"
#include "event2/listener.h"
#include "event2/util.h"
#include "event2/event.h"
#include "event2/event-config.h"
#include <WinSock2.h>

#pragma pack(1)

using namespace std;

#define IP_ADDRESS ("127.0.0.1")
#define PORT (7788)

char code[32] = "00700";
char name[16] = "腾讯";
int countNo ;

//发送数据格式
struct Header
{
	int				m_intPackSize;	//传送包大小
	int				m_intPackNo;	//包序号
};

struct Body
{
	int DataSize;		//数据包大小
	int mDate;
	int mTime;
	char Name[16];		
	char Code[32];	
	Body()
	{
		memset(this, 0, sizeof(Body));
	}
};


void accept_cb(int fd, short events, void* arg);
void socket_read_cb(int fd, short events, void *arg);
int tcp_server_init(int port, int listen_num);

int main(int argc, char** argv)
{
	//加载套接字库  
	WSADATA wsaData;
	int iRet = 0;
	countNo = 0;
	iRet = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iRet != 0)
	{
		//cout << "WSAStartup(MAKEWORD(2, 2), &wsaData) execute failed!" << endl;;
		return -1;
	}
	if (2 != LOBYTE(wsaData.wVersion) || 2 != HIBYTE(wsaData.wVersion))
	{
		WSACleanup();
		//cout << "WSADATA version is not correct!" << endl;
		return -1;
	}
	int listener = tcp_server_init(PORT, 10);
	if (listener == -1)
	{
		perror(" tcp_server_init error ");
		return -1;
	}
	struct event_base* base = event_base_new();
	//添加监听客户端请求连接事件  
	struct event* ev_listen = event_new(base, listener, EV_READ | EV_PERSIST,
		accept_cb, base);
	event_add(ev_listen, NULL);
	event_base_dispatch(base);
	WSACleanup();
	return 0;
}

void accept_cb(int fd, short events, void* arg)
{
	evutil_socket_t sockfd;
	struct sockaddr_in client;
	socklen_t len = sizeof(client);
	sockfd = ::accept(fd, (struct sockaddr*)&client, &len);
	evutil_make_socket_nonblocking(sockfd);
	printf("accept a client %d\n", sockfd);
	struct event_base* base = (event_base*)arg;
	//仅仅是为了动态创建一个event结构体  
	struct event *ev = event_new(NULL, -1, 0, NULL, NULL);
	//将动态创建的结构体作为event的回调参数  
	event_assign(ev, base, sockfd, EV_READ | EV_PERSIST, socket_read_cb, (void*)ev);
	event_add(ev, NULL);
}

template<typename Tp>
void AddToBuf(const Tp & data, vector<char> & vectPack, const bool blnBegin = false)
{
	char * pBuf = (char *)&data;
	if (blnBegin)
	{
		vectPack.insert(vectPack.begin(), pBuf, pBuf + sizeof(Tp));
	}
	else
	{
		vectPack.insert(vectPack.end(), pBuf, pBuf + sizeof(Tp));
	}
}

void socket_read_cb(int fd, short events, void *arg)
{
	char remsg[1024],msg[1024];
	struct event *ev = (struct event*)arg;
	int len = recv(fd, remsg, sizeof(remsg) - 1, 0);
	if (len <= 0)
	{
		printf("some error happen when read\n");
		event_free(ev);
		closesocket(fd);
		return;
	}
	remsg[len] = '\0';
	printf("recv the client msg: %s\n", remsg);
	if (!strcmp(remsg,"OK"))
	{
		cout << "Send Msg!" << endl;
		
		vector<char> buf;
		Header pHead;
		Body pBody;
		
		memcpy(pBody.Code,"00700",strlen(code));
		memcpy(pBody.Name,"Tcent",strlen(name));
		pBody.mDate = 20180827;
		pBody.mTime = 140432;
		pBody.DataSize = sizeof(pBody);

		
		pHead.m_intPackNo = countNo;
		pHead.m_intPackSize = sizeof(pHead)+sizeof(pBody);


		AddToBuf(pHead, buf);
		AddToBuf(pBody, buf);
		memcpy(msg,&*buf.begin(), buf.size() );

		int ret = send(fd, msg, sizeof(msg), 0);

	}
	countNo++;
	memset(msg, 0, sizeof(msg));
	/*char reply_msg[4096] = "I have recvieced the msg: ";
	strcat(reply_msg + strlen(reply_msg), msg);
	int ret = send(fd, reply_msg, strlen(reply_msg), 0);*/
}

int tcp_server_init(int port, int listen_num)
{
	int errno_save;
	int listener;
	listener = ::socket(AF_INET, SOCK_STREAM, 0);
	if (listener == -1)
		return -1;
	//允许多次绑定同一个地址。要用在socket和bind之间  
	evutil_make_listen_socket_reuseable(listener);
	struct sockaddr_in sin;
	sin.sin_family = AF_INET;
	sin.sin_addr.s_addr = 0;
	sin.sin_port = htons(port);
	if (::bind(listener, (struct sockaddr*)&sin, sizeof(sin)) < 0) {
		errno_save = errno;
		evutil_closesocket(listener);
		errno = errno_save;
		return -1;
	}
	if (::listen(listener, listen_num) < 0) {
		errno_save = errno;
		evutil_closesocket(listener);
		errno = errno_save;
		return -1;
	}
	//跨平台统一接口，将套接字设置为非阻塞状态  
	evutil_make_socket_nonblocking(listener);
	return listener;
}
